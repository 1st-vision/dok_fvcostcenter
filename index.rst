.. |label| replace:: Kostenstelle
.. |snippet| replace:: FvCostCenter
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.3.4
.. |maxVersion| replace:: 5.3.4
.. |Version| replace:: 0.5.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis


Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Beschreibung
------------
Dieses Plugin dient zur Erstellung und Bearbeitung von Kostenstellen der Kunden. Die Administration der Kostenstelle wird über das Frontend durchgeführt.

Frontend
--------
Unter „Mein Konto“ wir ein Reiter angezeigt namens „Meine Kostenstellen“.

.. image:: FvCostCenter1.png

Mit dem Button „Hinzufügen“ können Sie neue Kostenstellen anlegen.

.. image:: FvCostCenter2.png

Unter User werden die Shop-Konten angezeigt.

Sie können hier eine neue Kostenstelle anlegen mit einer Bezeichnung und ein Shop-Konto hinterlegen. Es werden alle Shopkonto mit der selben Kundennummer angezeigt.

! Besonderheit !
mit dem Plugin FvUserPermission werden bei User die Mitarbeiter angezeigt und können somit zugeordnet werden. Und es muss das entsprechende Recht der Anwender haben.

In der Kostenstellenansicht können Sie jederzeit die bereits erstellten Kostenstellen bearbeiten und User zuordnen.

.. image:: FvCostCenter3.png

Sie können auch in die USER-Ansicht wechseln

.. image:: FvCostCenter4.png

Hier werden die Kostenstellen pro USER angezeigt und Sie können auch ein Standard dem USER hinterlegen.

Die Kostenstelle die Sie als Standard dem USER hinterlegen wird auch in der Kaufabwicklung dies als default hinterlegt. Es werden auch nur die Kostenstellen angezeigt die dem USER hinterlegt sind.

.. image:: FvCostCenter5.png

! Besonderheit !
mit dem Plugin FvUserPermission muss der USER auch das Recht dazu haben.

Backend
-------


technische Beschreibung
------------------------
fv_cost_center: Hier werden die Kostenstellen und die Bezeichnung hinterlegt, dazu der Shopware-Account
fv_cost_center_relations : Hier werden die Verknüpfung von USER und KostenstelleID hinterlegt.

Bei einer Bestellung werden folgende Werte hinterlegt:
Tabelle: s_order_attributes
fv_cost_center_name: Es wird die Bezeichnung der Kostenstelle hinterlegt
fv_cost_center_code: Es wird die Kostenstelle hinterlegt


Modifizierte Template-Dateien
-----------------------------
:/account/sidebar.tpl:
:/checkout/confirm.tpl:


kompatibel mit anderen Plugins
------------------------------

:User-Berechtigungen für Konzerne: FvUserPermissions

